import urllib, re
import nltk

class Analyzer:
    # Parameters that should be passed to create a class
    links = ['http://www.delfi.lt/verslas/energetika/nafta-brangs-opec-salys-sutare-mazinti-naftos-gavybos-apimtis-po-8-metu-pertraukos.d?id=72426796']
    keywords = []
    # filters = ['/<div>/', '/<!doctype>/', '/<footer>/', '/<script>/', '/<meta>/', '/<a>/', '/<li>/', '/</ul>/', '/</div>/', '/<a /', '/js/']
    filters = ['/nafta+/.', '/augimas/', '/OPEC/']
    generation = 0     # Initially should be zero
    recognition = False # Bool
    minedText = []
    output = []

    def __init__(self):
        for link in Analyzer.links:
            response = self.makeRequest(link)
            # for line in response:
            pageContent = self.cleanHTML(response)
            self.analyzeContent(pageContent, Analyzer.filters)
            self.cleanHTML(pageContent)

    def makeRequest(self, link):
        response = urllib.urlopen(link)
        return response.read()

    def analyzeContent(self, content, filters):
        for word in filters:
            if re.findall(word, content):
                break
                # print 'Skipped output: ' + content
            else:
                self.saveFiltered(content)

    def saveFiltered(self, input):
        Analyzer.output.append(input)
        print 'Output saved: ' + input

    def matchCheck(self, input):
        match = Analyzer.recognition
        for words in Analyzer.filters:
            if input == words:
                Analyzer.minedText.append(input)
                match = True

    def cleanHTML(self, content):
        raw = nltk.clean_html(content)
        print raw



analyzed = Analyzer()
# print Analyzer.output
