import requests, json
import matplotlib.pyplot as plot
import matplotlib.dates as mdates
import datetime as dt

class Data:

    def __init__(self, name, url, key):
        self.name = name
        self.url = url
        self.key = 'api_key=' + key
        self.values = []
        self.dates = []
        self.request = requests.get(self.url, self.key)
        self.textConvert()

    def textConvert(self):
        response = json.loads(self.request.text)
        for key in response['dataset']['data']:
            print key
            self.values.append(key[1])
            self.dates.append(key[0])
        self.graphGenerator(self.dates, self.values)

    def graphGenerator(self, x, y):
        plot.title(self.name)
        dates = [dt.datetime.strptime(d, '%Y-%m-%d').date() for d in x]
        plot.plot(dates, y)
        plot.show()

unemployment = Data('Unemployment', 'https://www.quandl.com/api/v3/datasets/ULMI/YUNEM_M_NOR.json', 'RDXDV2a6_x79uuHH2c8A')
oil = Data('Oil Price', 'https://www.quandl.com/api/v3/datasets/OPEC/ORB.json', 'RDXDV2a6_x79uuHH2c8A')